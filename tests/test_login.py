from django.test import TestCase, override_settings
from django.urls import reverse

from blender_id_oauth_client.views import blender_id_oauth_settings


class LoginTestCase(TestCase):
    def test_login_view_redirect(self):
        protected_url = reverse('protected-view')
        r = self.client.get(protected_url)
        self.assertEqual(r.status_code, 302)
        self.client.get(r.url)
        self.assertEqual(self.client.session.get('next_after_login'), protected_url)


class ClientSettingsTestCase(TestCase):
    """Test the blender_id_oauth_settings() function."""

    def tearDown(self):
        blender_id_oauth_settings.cache_clear()
        super().tearDown()

    @override_settings(BLENDER_ID={
        'BASE_URL': 'http://id.local:8000',  # note lack of trailing slash
        'OAUTH_CLIENT': 'client',
        'OAUTH_SECRET': 'secret'
    })
    def test_url_root_assert(self):
        """Test a BLENDER_ID base URL not ending in slash."""
        blender_id_oauth_settings.cache_clear()
        c = blender_id_oauth_settings()
        self.assertEqual('http://id.local:8000/oauth/authorize', c.url_authorize)

    @override_settings(BLENDER_ID={
        'BASE_URL': 'http://id.local:8000/',
        'OAUTH_CLIENT': 'client',
        'OAUTH_SECRET': 'secret'
    })
    def test_happy_client_config(self):
        """Test a correctly configured BLENDER_ID"""
        blender_id_oauth_settings.cache_clear()
        c = blender_id_oauth_settings()
        self.assertEqual('client', c.client)
        self.assertEqual('secret', c.secret)
        self.assertEqual('http://id.local:8000/', c.url_base)
        self.assertEqual('http://id.local:8000/oauth/authorize', c.url_authorize)
        self.assertEqual('http://id.local:8000/oauth/token', c.url_token)
        self.assertEqual('http://id.local:8000/api/me', c.url_userinfo)
        self.assertEqual('http://id.local:8000/logout', c.url_logout)
